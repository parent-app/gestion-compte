CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

INSERT INTO t_user(username, email, password, role, external_id) VALUES
('admin', 'admin@email.com', '$2y$10$8owkKhezhrpP1fMuraU4rOzugIOGhWVnNMVg6JYt5BQgYrEP0PCka', 'ROLE_ADMIN', uuid_generate_v1()),
('user', 'user@email.com', '$2y$10$ietuIFlfxK5h3d3VkjBZ8Obc498XYz5Ub0XfdnXMcyJUAsOQRFJQG', 'ROLE_USER', uuid_generate_v1());
