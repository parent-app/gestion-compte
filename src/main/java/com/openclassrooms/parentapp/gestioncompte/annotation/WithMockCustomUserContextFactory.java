package com.openclassrooms.parentapp.gestioncompte.annotation;

import com.openclassrooms.parentapp.gestioncompte.entity.ERole;
import com.openclassrooms.parentapp.gestioncompte.security.UserDetailsImpl;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.util.List;

public class WithMockCustomUserContextFactory implements WithSecurityContextFactory<WithMockCustomUser> {
    @Override
    public SecurityContext createSecurityContext(WithMockCustomUser withMockCustomUser) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();

        UserDetailsImpl principal = UserDetailsImpl.builder()
                .password("password")
                .email("test@email.com")
                .username("test")
                .id("af443d23-b90e-41e0-8745-e0dc15fb4123")
                .authorities(List.of(new SimpleGrantedAuthority(ERole.ROLE_USER.name())))
                .build();

        Authentication auth = new UsernamePasswordAuthenticationToken(principal, null, principal.getAuthorities());
        context.setAuthentication(auth);
        return context;
    }
}
