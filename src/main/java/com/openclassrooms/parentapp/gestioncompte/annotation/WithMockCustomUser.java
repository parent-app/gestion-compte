package com.openclassrooms.parentapp.gestioncompte.annotation;

import com.openclassrooms.parentapp.gestioncompte.entity.ERole;
import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockCustomUserContextFactory.class)
public @interface WithMockCustomUser {
    String username() default "test";
    String password() default "test";
    String email() default "test@email.com";
    String id() default "af443d23-b90e-41e0-8745-e0dc15fb4123";
    ERole role() default ERole.ROLE_USER;
}
