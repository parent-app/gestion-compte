package com.openclassrooms.parentapp.gestioncompte.repository;

import com.openclassrooms.parentapp.gestioncompte.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    User findByEmail(String email);
    Optional<User> findByExternalId(UUID externalId);
}
