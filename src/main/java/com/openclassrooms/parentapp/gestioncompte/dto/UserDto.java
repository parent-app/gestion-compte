package com.openclassrooms.parentapp.gestioncompte.dto;

import com.openclassrooms.parentapp.gestioncompte.entity.ERole;
import com.openclassrooms.parentapp.gestioncompte.exception.ValidationErrorMessage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private Long id;

    private UUID externalId;

    @NotBlank(message = ValidationErrorMessage.USERNAME_REQUIRED)
    @Size(max = 20, message = ValidationErrorMessage.USERNAME_SIZE)
    private String username;

    @NotBlank(message = ValidationErrorMessage.EMAIL_REQUIRED)
    @Email(message = ValidationErrorMessage.EMAIL_PATTERN)
    private String email;

    @NotBlank(message = ValidationErrorMessage.PASSWORD_REQUIRED)
    @Size(max = 20, message = ValidationErrorMessage.PASSWORD_SIZE)
    private String password;

    private ERole role;

    private List<ChildDto> children;
}
