package com.openclassrooms.parentapp.gestioncompte.dto;

import com.openclassrooms.parentapp.gestioncompte.exception.ValidationErrorMessage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChildDto {

    private Long id;

    @NotBlank(message = ValidationErrorMessage.CHILD_NAME_REQUIRED)
    private String name;

    @NotNull(message = ValidationErrorMessage.CHILD_BIRTHDATE_REQUIRED)
    private LocalDate birthDate;

    @NotNull(message = ValidationErrorMessage.PARENT_ID_REQUIRED)
    private UUID userId;
}
