package com.openclassrooms.parentapp.gestioncompte.dto;

import com.openclassrooms.parentapp.gestioncompte.entity.ERole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDtoWithoutPassword {

    private Long id;
    private String username;
    private String email;
    private ERole role;
}
