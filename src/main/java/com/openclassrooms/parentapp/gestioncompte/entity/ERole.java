package com.openclassrooms.parentapp.gestioncompte.entity;

public enum ERole {
    ROLE_ADMIN,
    ROLE_USER
}
