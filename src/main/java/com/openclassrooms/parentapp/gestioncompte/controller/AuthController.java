package com.openclassrooms.parentapp.gestioncompte.controller;

import com.openclassrooms.parentapp.gestioncompte.dto.UserDto;
import com.openclassrooms.parentapp.gestioncompte.dto.UserDtoWithoutPassword;
import com.openclassrooms.parentapp.gestioncompte.entity.JwtResponse;
import com.openclassrooms.parentapp.gestioncompte.entity.LoginRequest;
import com.openclassrooms.parentapp.gestioncompte.security.JwtUtils;
import com.openclassrooms.parentapp.gestioncompte.security.UserDetailsImpl;
import com.openclassrooms.parentapp.gestioncompte.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(AuthController.ROOT_RESOURCE)
@RequiredArgsConstructor
@Tag(name = "auth", description = "Controller to handle authentication")
@CrossOrigin(origins = "*")
public class AuthController {

    public static final String ROOT_RESOURCE = "/api/auth";
    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;

    /**
     * Method to authenticate a user and update SecurityContext
     * URL : /api/auth/signin
     *
     * @param loginRequest the login request which contains username and password
     * @return ResponseEntity which contains JWTT and UserDetails
     *
     * @see LoginRequest
     * @see AuthenticationManager#authenticate(Authentication)
     * @see JwtUtils#generateJwtToken(Authentication)
     */
    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        return ResponseEntity.ok(
                JwtResponse.builder()
                        .token(jwt)
                        .id(userDetails.getId())
                        .username(userDetails.getUsername())
                        .email(userDetails.getEmail())
                        .roles(roles).build()
        );
    }
    /**
     * Method to register a new user : create it and save it to database
     * URL : /api/auth/signup
     *
     * @param userDto the signup request that contains the information needed to register a user
     * @return MessageResponse which confirms the creation of the user
     */
    @PostMapping("/signup")
    public UserDtoWithoutPassword registerUser(@Valid @RequestBody UserDto userDto) {
        return userService.create(userDto);
    }



}
