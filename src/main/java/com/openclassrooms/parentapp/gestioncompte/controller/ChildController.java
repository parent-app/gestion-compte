package com.openclassrooms.parentapp.gestioncompte.controller;

import com.openclassrooms.parentapp.gestioncompte.dto.ChildDto;
import com.openclassrooms.parentapp.gestioncompte.service.ChildService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(ChildController.ROOT_RESOURCE)
@RequiredArgsConstructor
@Tag(name = "children", description = "Controller to handle user's children requests")
@CrossOrigin(origins = "*")
public class ChildController {

    public static final String ROOT_RESOURCE = "/api/children";
    private final ChildService childService;

    @GetMapping
    @Operation(summary = "Get all user's children")
    @PreAuthorize("hasRole('ADMIN') or #userId == authentication.principal.id")
    public List<ChildDto> getUserChildren(@Parameter(name = "userId", description = "User id") @RequestParam String userId) {
        return childService.getAllUserChildren(userId);
    }

    @PostMapping
    @Operation(summary = "Create a child")
    @RolesAllowed({"ADMIN", "USER"})
    public ChildDto createChild(@Valid @RequestBody ChildDto childDto) {
        return childService.create(childDto);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update a child")
    @PreAuthorize("hasRole('ADMIN') or #childDto.userId.toString() == authentication.principal.id")
    public ChildDto updateChild(@RequestBody @Valid ChildDto childDto) {
        return childService.update(childDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete child")
    @RolesAllowed({"ADMIN", "USER"})
    public ResponseEntity<Void> deleteChild(@Parameter(name = "id", description = "Child id") @PathVariable Long id) {
        childService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
