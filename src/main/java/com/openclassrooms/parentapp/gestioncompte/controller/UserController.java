package com.openclassrooms.parentapp.gestioncompte.controller;

import com.openclassrooms.parentapp.gestioncompte.dto.UserDto;
import com.openclassrooms.parentapp.gestioncompte.dto.UserDtoWithoutPassword;
import com.openclassrooms.parentapp.gestioncompte.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.UUID;

/**
 * Controller to handle user requests
 */
@RestController
@RequestMapping(UserController.ROOT_RESOURCE)
@RequiredArgsConstructor
@Tag(name = "users", description = "Controller to handle user requests")
@CrossOrigin(origins = "*")
public class UserController {

    public static final String ROOT_RESOURCE = "/api/users";
    private final UserService userService;

    @GetMapping
    @Operation(summary = "Get pages of users")
    @RolesAllowed("ADMIN")
    public Page<UserDtoWithoutPassword> getUsers(@Parameter(name = "page", description = "The page number to display. Default value : 0") @RequestParam(defaultValue = "0") Integer page,
                                                 @Parameter(name = "size", description = "Number of items per page. Default value : 20") @RequestParam(defaultValue = "20") Integer size,
                                                 @Parameter(name = "sortBy", description = "Sort attribute. Default value : username") @RequestParam(defaultValue = "username") String sortBy,
                                                 @Parameter(name = "direction", description = "Sort direction. Default value : ASC") @RequestParam(defaultValue = "DESC") Sort.Direction direction,
                                                 @Parameter(name = "unpaged", description = "Boolean allowing pagination to be disabled. Default value : false") @RequestParam(defaultValue = "false") boolean unpaged) {
        Pageable pageable = unpaged ? Pageable.unpaged() : PageRequest.of(page, size, direction, sortBy);
        return userService.findAll(pageable);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get a user by id")
    @PreAuthorize("hasRole('ADMIN') or #id.toString() == authentication.principal.id")
    public UserDto getUserByExternalId(@Parameter(name = "id", description = "external id of the requested user") @PathVariable UUID id) {
        return userService.findById(id);
    }

    @PostMapping
    @Operation(summary = "Create a user")
    @RolesAllowed("ADMIN")
    public UserDtoWithoutPassword createUser(@RequestBody @Valid UserDto userDto) {
        return userService.create(userDto);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update a user")
    @PreAuthorize("hasRole('ADMIN') or #userDto.externalId.toString() == authentication.principal.id")
    public UserDtoWithoutPassword updateUser(@Parameter(name = "id", description = "user id") @PathVariable Long id,
                                        @RequestBody @Valid UserDto userDto) {
        return userService.update(userDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a user")
    @RolesAllowed("ADMIN")
    public ResponseEntity<Void> deleteUser(@Parameter(name = "id", description = "User id") @PathVariable Long id) {
        userService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
