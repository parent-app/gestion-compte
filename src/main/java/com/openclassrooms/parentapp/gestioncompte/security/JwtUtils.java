package com.openclassrooms.parentapp.gestioncompte.security;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * JWT utility class to generate and validate JWT, and get username from JWT
 */
@Slf4j
@Component
public class JwtUtils {

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationMs}")
    private int jwtExpirationMs;

    /**
     * Method to generate a JWT token from usename, date, expiration date and secret
     *
     * @param authentication Authentication obejct to get username
     *
     * @return JWT
     */
    public String generateJwtToken(Authentication authentication) {
        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
        return Jwts.builder()
                .setSubject(userPrincipal.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + jwtExpirationMs))
                .claim("extId", userPrincipal.getId())
                .claim("role", userPrincipal.getAuthorities().stream().findFirst().get().toString())
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    /**
     * To get username from JWT token
     *
     * @param token JWT token
     * @return username
     */
    public String getUsernameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    /**
     * Method to validate JWT token
     *
     * @param token token to validate
     * @return boolean
     */
    public boolean validateJwtToken(String token) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
            return true;
        } catch (SignatureException e) {
            log.error("JWT signature invalide : {}", e.getMessage());
        } catch (MalformedJwtException e) {
            log.error("JWT token invalide : {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            log.error("JWT token expiré : {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            log.error("JWT token non supporté : {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.error("JWT token vide : {}", e.getMessage());
        }
        return false;
    }
}

