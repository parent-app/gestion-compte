package com.openclassrooms.parentapp.gestioncompte.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.openclassrooms.parentapp.gestioncompte.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * User's information for Authentication
 */
@AllArgsConstructor
@Data
@Builder
public class UserDetailsImpl implements UserDetails {

    private String id;

    private String username;

    private String email;

    @JsonIgnore
    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    /**
     * Method to build a UserDetailsImpl object from a user. We convert user role into list of GrantedAuthority
     * to facilitate the work with Spring Security and Authentication
     *
     * @param user user
     * @return UserDetailsImpl
     */
    public static UserDetailsImpl build(User user) {
        return new UserDetailsImpl(
                user.getExternalId().toString(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                List.of(new SimpleGrantedAuthority(user.getRole().name()))
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
