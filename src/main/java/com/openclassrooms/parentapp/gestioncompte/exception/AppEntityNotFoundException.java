package com.openclassrooms.parentapp.gestioncompte.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AppEntityNotFoundException extends RuntimeException {

    public AppEntityNotFoundException() {
        super("Entité non trouvée");
    }
}
