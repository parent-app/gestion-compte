package com.openclassrooms.parentapp.gestioncompte.exception;

public class ValidationErrorMessage {
    public static final String USERNAME_SIZE = "Le pseudo de l'utilisateur doit contenir maximum 20 caractères";
    public static final String USERNAME_REQUIRED = "Le pseudo de l'utilisateur est requis";
    public static final String EMAIL_REQUIRED = "L'email de l'utilisateur est requis";
    public static final String  EMAIL_PATTERN = "L'email n'est pas conforme";
    public static final String PASSWORD_REQUIRED = "Le mot de passe de l'utilisateur est obligatoire";
    public static final String PASSWORD_SIZE = "Le mot de passe de l'utilisateur doit contenir maximum 20 caractères";
    public static final String CHILD_NAME_REQUIRED = "Le nom de l'enfant est requis";
    public static final String CHILD_BIRTHDATE_REQUIRED = "La date de naissance de l'enfant est obligatoire";
    public static final String PARENT_ID_REQUIRED = "L'identifiant du parent est obligatoire";

    private ValidationErrorMessage() {
        // private constructor
    }
}
