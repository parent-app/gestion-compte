package com.openclassrooms.parentapp.gestioncompte.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class EmailExistsException extends ResponseStatusException {

    public EmailExistsException(String email) {
        super(HttpStatus.BAD_REQUEST, String.format("L'adresse email %s est déjà utilisée par un autre compte", email));
    }
}
