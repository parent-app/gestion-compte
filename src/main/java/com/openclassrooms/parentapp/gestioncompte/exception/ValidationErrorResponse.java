package com.openclassrooms.parentapp.gestioncompte.exception;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ValidationErrorResponse {

    private final String fieldName;
    private final String message;
}
