package com.openclassrooms.parentapp.gestioncompte.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class UsernameExistsException extends ResponseStatusException {

    public UsernameExistsException(String username) {
        super(HttpStatus.BAD_REQUEST, String.format("Le nom d'utilisateur %s est déjà utilisé", username));
    }
}
