package com.openclassrooms.parentapp.gestioncompte.mapper;

import com.openclassrooms.parentapp.gestioncompte.dto.UserDto;
import com.openclassrooms.parentapp.gestioncompte.dto.UserDtoWithoutPassword;
import com.openclassrooms.parentapp.gestioncompte.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = ChildMapper.class)
public interface UserMapper {
    UserDtoWithoutPassword toUserDtoWithoutPassword(User user);

    @Mapping(target = "password", ignore = true)
    UserDto toUserDto(User user);
}
