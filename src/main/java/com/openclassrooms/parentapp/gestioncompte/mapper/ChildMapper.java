package com.openclassrooms.parentapp.gestioncompte.mapper;

import com.openclassrooms.parentapp.gestioncompte.dto.ChildDto;
import com.openclassrooms.parentapp.gestioncompte.entity.Child;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ChildMapper {

    @Mapping(target = "parent", ignore = true)
    Child toChild(ChildDto childDto);

    @Mapping(target = "userId", source = "parent.externalId")
    ChildDto toChildDto(Child child);

    List<ChildDto> toChildDtoList(List<Child> children);
}
