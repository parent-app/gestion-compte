package com.openclassrooms.parentapp.gestioncompte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class ParentAppGestionCompteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParentAppGestionCompteApplication.class, args);
	}

}
