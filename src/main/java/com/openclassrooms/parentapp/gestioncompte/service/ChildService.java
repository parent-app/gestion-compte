package com.openclassrooms.parentapp.gestioncompte.service;

import com.openclassrooms.parentapp.gestioncompte.dto.ChildDto;
import com.openclassrooms.parentapp.gestioncompte.exception.AppEntityNotFoundException;
import com.openclassrooms.parentapp.gestioncompte.mapper.ChildMapper;
import com.openclassrooms.parentapp.gestioncompte.repository.ChildRepository;
import com.openclassrooms.parentapp.gestioncompte.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ChildService {

    private final ChildRepository childRepository;

    private final ChildMapper childMapper;

    private final UserRepository userRepository;

    public List<ChildDto> getAllUserChildren(String userId) {
        var user = userRepository.findByExternalId(UUID.fromString(userId)).orElseThrow(AppEntityNotFoundException::new);
        return childMapper.toChildDtoList(childRepository.findAllByParentId(user.getId()));
    }

    public ChildDto create(ChildDto childDto) {
        var child = childMapper.toChild(childDto);
        var user = userRepository.findByExternalId(childDto.getUserId()).orElseThrow(AppEntityNotFoundException::new);
        child.setParent(user);
        return childMapper.toChildDto(childRepository.save(child));
    }

    public ChildDto update(ChildDto childDto) {
        var child = childRepository.findById(childDto.getId()).orElseThrow(AppEntityNotFoundException::new);
        child.setName(childDto.getName());
        child.setBirthDate(childDto.getBirthDate());
        return childMapper.toChildDto(childRepository.save(child));
    }

    public void delete(Long id) {
        var child = childRepository.findById(id).orElseThrow(AppEntityNotFoundException::new);
        childRepository.delete(child);
    }
}
