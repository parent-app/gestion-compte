package com.openclassrooms.parentapp.gestioncompte.service;

import com.openclassrooms.parentapp.gestioncompte.dto.UserDto;
import com.openclassrooms.parentapp.gestioncompte.dto.UserDtoWithoutPassword;
import com.openclassrooms.parentapp.gestioncompte.entity.ERole;
import com.openclassrooms.parentapp.gestioncompte.entity.User;
import com.openclassrooms.parentapp.gestioncompte.exception.AppEntityNotFoundException;
import com.openclassrooms.parentapp.gestioncompte.exception.EmailExistsException;
import com.openclassrooms.parentapp.gestioncompte.exception.UsernameExistsException;
import com.openclassrooms.parentapp.gestioncompte.mapper.UserMapper;
import com.openclassrooms.parentapp.gestioncompte.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService {

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public Page<UserDtoWithoutPassword> findAll(Pageable pageable) {
        return userRepository.findAll(pageable).map(userMapper::toUserDtoWithoutPassword);
    }

    public UserDto findById(UUID id) {
        return userMapper.toUserDto(userRepository.findByExternalId(id).orElseThrow(AppEntityNotFoundException::new));
    }

    public UserDtoWithoutPassword create(UserDto userDto) throws EmailExistsException, UsernameExistsException {
        checkIfUsernameExists(userDto);
        checkIfEmailExists(userDto);
        ERole role = userDto.getRole() != null ? userDto.getRole() : ERole.ROLE_USER;
        var user = User.builder()
                .username(userDto.getUsername())
                .password(passwordEncoder.encode(userDto.getPassword()))
                .email(userDto.getEmail())
                .role(role)
                .externalId(UUID.randomUUID())
                .build();
        return userMapper.toUserDtoWithoutPassword(userRepository.save(user));
    }

    public UserDtoWithoutPassword update(UserDto userDto) throws EmailExistsException, UsernameExistsException {
        checkIfUsernameExists(userDto);
        checkIfEmailExists(userDto);
        var user = userRepository.findById(userDto.getId()).orElseThrow(AppEntityNotFoundException::new);
        user.setEmail(userDto.getEmail());
        user.setUsername(userDto.getUsername());
        user.setRole(userDto.getRole());
        return userMapper.toUserDtoWithoutPassword(userRepository.save(user));
    }

    public void delete(Long id) {
        var user = userRepository.findById(id).orElseThrow(AppEntityNotFoundException::new);
        userRepository.delete(user);
    }

    private void checkIfUsernameExists(UserDto userDto) throws UsernameExistsException {
        Optional<User> user = userRepository.findByUsername(userDto.getUsername());
        if (user.isPresent() && !user.get().getId().equals(userDto.getId())) {
            throw new UsernameExistsException(userDto.getUsername());
        }
    }

    private void checkIfEmailExists(UserDto userDto) throws EmailExistsException {
        var user = userRepository.findByEmail(userDto.getEmail());
        if (user != null && !user.getId().equals(userDto.getId())) {
            throw new EmailExistsException(userDto.getEmail());
        }
    }
}
