package com.openclassrooms.parentapp.gestioncompte.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.parentapp.gestioncompte.annotation.WithMockCustomUser;
import com.openclassrooms.parentapp.gestioncompte.dto.ChildDto;
import com.openclassrooms.parentapp.gestioncompte.entity.Child;
import com.openclassrooms.parentapp.gestioncompte.entity.ERole;
import com.openclassrooms.parentapp.gestioncompte.entity.User;
import com.openclassrooms.parentapp.gestioncompte.exception.ValidationErrorMessage;
import com.openclassrooms.parentapp.gestioncompte.fixture.ChildFixture;
import com.openclassrooms.parentapp.gestioncompte.mapper.ChildMapper;
import com.openclassrooms.parentapp.gestioncompte.repository.ChildRepository;
import com.openclassrooms.parentapp.gestioncompte.repository.UserRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ChildControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ChildRepository childRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChildMapper childMapper;

    @Autowired
    private ObjectMapper objectMapper;

    private User user;

    @BeforeAll
    public void initUser() {
        user = userRepository.save(User.builder()
                .username("test")
                .password("password")
                .email("test@email.com")
                .externalId(UUID.fromString("af443d23-b90e-41e0-8745-e0dc15fb4123"))
                .role(ERole.ROLE_USER)
                .build());
    }

    @BeforeEach
    @AfterEach
    public void purgeChild() {
        childRepository.deleteAll();
    }

    @AfterAll
    public void purgeUser() {
        userRepository.deleteAll();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getUserChildren() throws Exception {
        // GIVEN
        List<Child> children = childRepository.saveAll(ChildFixture.buildChildList(user));
        List<ChildDto> childrenDto = childMapper.toChildDtoList(children);

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(ChildController.ROOT_RESOURCE)
                .param("userId", user.getExternalId().toString()))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn();
    }

    @Test
    @WithMockCustomUser
    void getUserChildren_roleUser_succeed() throws Exception {
        // GIVEN
        List<Child> children = childRepository.saveAll(ChildFixture.buildChildList(user));
        List<ChildDto> childrenDto = childMapper.toChildDtoList(children);

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(ChildController.ROOT_RESOURCE)
                .param("userId", user.getExternalId().toString()))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn();
    }

    @Test
    @WithMockCustomUser
    void getUserChildren_forbidden() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(ChildController.ROOT_RESOURCE)
                .param("userId", UUID.randomUUID().toString()))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createChild() throws Exception {
        // GIVEN
        ChildDto childDto = childMapper.toChildDto(ChildFixture.buildChild(user));

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(ChildController.ROOT_RESOURCE)
                .content(objectMapper.writeValueAsString(childDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.name", is(childDto.getName())))
                .andExpect(jsonPath("$.birthDate", is(childDto.getBirthDate().toString())))
                .andExpect(jsonPath("$.userId", is(user.getExternalId().toString())))
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "VIEWER")
    void createChild_forbidden() throws Exception {
        // GIVEN
        ChildDto childDto = childMapper.toChildDto(ChildFixture.buildChild(user));

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(ChildController.ROOT_RESOURCE)
                .content(objectMapper.writeValueAsString(childDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateChild() throws Exception {
        // GIVEN
        Child child = childRepository.save(ChildFixture.buildChild(user));
        child.setName("updated name");
        ChildDto childDto = childMapper.toChildDto(child);

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.put(ChildController.ROOT_RESOURCE + "/" + child.getId())
                .content(objectMapper.writeValueAsString(childDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.name", is(child.getName())))
                .andExpect(jsonPath("$.id", is(child.getId().intValue())))
                .andReturn();
    }

    @Test
    @WithMockCustomUser
    void updateChild_roleUser_succeed() throws Exception {
        // GIVEN
        Child child = childRepository.save(ChildFixture.buildChild(user));
        child.setName("updated name");
        ChildDto childDto = childMapper.toChildDto(child);

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.put(ChildController.ROOT_RESOURCE + "/" + child.getId())
                .content(objectMapper.writeValueAsString(childDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.name", is(child.getName())))
                .andExpect(jsonPath("$.id", is(child.getId().intValue())))
                .andReturn();
    }

    @Test
    @WithMockCustomUser
    void updateChild_forbidden() throws Exception {
        // GIVEN
        Child child = childRepository.save(ChildFixture.buildChild(user));
        child.setName("updated name");
        ChildDto childDto = childMapper.toChildDto(child);
        childDto.setUserId(UUID.randomUUID());

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.put(ChildController.ROOT_RESOURCE + "/" + child.getId())
                .content(objectMapper.writeValueAsString(childDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteChild() throws Exception {
        // GIVEN
        Child child = childRepository.save(ChildFixture.buildChild(user));

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.delete(ChildController.ROOT_RESOURCE + "/" + child.getId())
                .content(objectMapper.writeValueAsString(child))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andReturn();

        assertThat(childRepository.findById(child.getId())).isNotPresent();
    }

    @Test
    @WithMockUser(roles = "VIEWER")
    void deleteChild_forbidden() throws Exception {
        // GIVEN
        Child child = childRepository.save(ChildFixture.buildChild(user));

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.delete(ChildController.ROOT_RESOURCE + "/" + child.getId())
                .content(objectMapper.writeValueAsString(child))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createChild_ValidationException() throws Exception {
        // GIVEN
        ChildDto childDto = ChildDto.builder()
                .birthDate(LocalDate.now())
                .userId(UUID.randomUUID())
                .build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(ChildController.ROOT_RESOURCE)
                .content(objectMapper.writeValueAsString(childDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.fieldName", is("name")))
                .andExpect(jsonPath("$.message", is(ValidationErrorMessage.CHILD_NAME_REQUIRED)))
                .andReturn();

        // GIVEN
        childDto.setName("child");
        childDto.setBirthDate(null);

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(ChildController.ROOT_RESOURCE)
                .content(objectMapper.writeValueAsString(childDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.fieldName", is("birthDate")))
                .andExpect(jsonPath("$.message", is(ValidationErrorMessage.CHILD_BIRTHDATE_REQUIRED)))
                .andReturn();

        // GIVEN
        childDto.setUserId(null);
        childDto.setBirthDate(LocalDate.now());

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(ChildController.ROOT_RESOURCE)
                .content(objectMapper.writeValueAsString(childDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.fieldName", is("userId")))
                .andExpect(jsonPath("$.message", is(ValidationErrorMessage.PARENT_ID_REQUIRED)))
                .andReturn();
    }
}
