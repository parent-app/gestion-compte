package com.openclassrooms.parentapp.gestioncompte.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.parentapp.gestioncompte.dto.UserDto;
import com.openclassrooms.parentapp.gestioncompte.entity.ERole;
import com.openclassrooms.parentapp.gestioncompte.entity.LoginRequest;
import com.openclassrooms.parentapp.gestioncompte.entity.User;
import com.openclassrooms.parentapp.gestioncompte.repository.ChildRepository;
import com.openclassrooms.parentapp.gestioncompte.repository.UserRepository;
import com.openclassrooms.parentapp.gestioncompte.security.JwtUtils;
import com.openclassrooms.parentapp.gestioncompte.service.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class AuthControllerIT {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChildRepository childRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ObjectMapper mapper;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
        userRepository.deleteAll();
    }

    @Test
    void authenticateUser() throws Exception {
        // GIVEN
        UserDto userDto = UserDto.builder()
                .username("username")
                .email("email@email.com")
                .password("password")
                .build();
        userService.create(userDto);
        LoginRequest loginRequest = new LoginRequest(userDto.getUsername(), "password");

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(AuthController.ROOT_RESOURCE + "/signin")
                .content(mapper.writeValueAsString(loginRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.username", is(userDto.getUsername())))
                .andExpect(jsonPath("$.token", any(String.class)))
                .andExpect(jsonPath("$.email", is(userDto.getEmail())))
                .andExpect(jsonPath("$.roles.[0]", is("ROLE_USER")))
                .andExpect(jsonPath("$.id", any(String.class)))
                .andReturn();
    }

    @Test
    void registerUser() throws Exception {
        // GIVEN
        UserDto userDto = UserDto.builder()
                .email("user@email.com")
                .username("username")
                .password("password")
                .build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(AuthController.ROOT_RESOURCE + "/signup")
                .content(mapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.username", is(userDto.getUsername())))
                .andReturn();
    }

    @Test
    void registerUser_usernameExists_badRequest() throws Exception {
        // GIVEN
        User user = User.builder()
                .username("username")
                .email("email@email.com")
                .password(passwordEncoder.encode("password"))
                .role(ERole.ROLE_USER)
                .build();
        userRepository.save(user);

        UserDto userDto = UserDto.builder()
                .email("user@email.com")
                .username("username")
                .password("password")
                .build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(AuthController.ROOT_RESOURCE + "/signup")
                .content(mapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(result -> Assertions.assertThat(result.getResponse().getErrorMessage()).isEqualTo("Le nom d'utilisateur " + userDto.getUsername() + " est déjà utilisé"))
                .andReturn();
    }

    @Test
    void registerUser_emailExists_badRequest() throws Exception {
        // GIVEN
        User user = User.builder()
                .username("username")
                .email("email@email.com")
                .password(passwordEncoder.encode("password"))
                .role(ERole.ROLE_USER)
                .build();
        userRepository.save(user);

        UserDto userDto = UserDto.builder()
                .email("email@email.com")
                .username("username2")
                .password("password")
                .build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(AuthController.ROOT_RESOURCE + "/signup")
                .content(mapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(result -> Assertions.assertThat(result.getResponse().getErrorMessage()).isEqualTo("L'adresse email " + userDto.getEmail() + " est déjà utilisée par un autre compte"))
                .andReturn();
    }
}
