package com.openclassrooms.parentapp.gestioncompte.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.parentapp.gestioncompte.annotation.WithMockCustomUser;
import com.openclassrooms.parentapp.gestioncompte.dto.UserDto;
import com.openclassrooms.parentapp.gestioncompte.entity.ERole;
import com.openclassrooms.parentapp.gestioncompte.entity.User;
import com.openclassrooms.parentapp.gestioncompte.exception.ValidationErrorMessage;
import com.openclassrooms.parentapp.gestioncompte.fixture.UserFixture;
import com.openclassrooms.parentapp.gestioncompte.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper mapper;

    private User user;

    @BeforeEach
    public void init() {
        userRepository.deleteAll();
        user = userRepository.save(User.builder()
                .username("test")
                .password("test")
                .email("test@email.com")
                .externalId(UUID.fromString("af443d23-b90e-41e0-8745-e0dc15fb4123"))
                .role(ERole.ROLE_USER)
                .build());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getUsers() throws Exception {
        // GIVEN
        List<User> users = userRepository.saveAll(UserFixture.buildUserList());

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(UserController.ROOT_RESOURCE))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.content.[0].id", is(users.get(2).getId().intValue())))
                .andExpect(jsonPath("$.content.[1].id", is(users.get(1).getId().intValue())))
                .andExpect(jsonPath("$.content.[2].id", is(users.get(0).getId().intValue())))
                .andReturn();
    }

    @Test
    @WithMockCustomUser
    void getUsers_forbidden() throws Exception {
        // GIVEN
        List<User> users = userRepository.saveAll(UserFixture.buildUserList());

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(UserController.ROOT_RESOURCE))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getUserByExternalId() throws Exception {
        // GIVEN
        User user = userRepository.save(UserFixture.buildUser());

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(UserController.ROOT_RESOURCE + "/" + user.getExternalId()))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.id", is(user.getId().intValue())))
                .andExpect(jsonPath("$.username", is(user.getUsername())))
                .andExpect(jsonPath("$.email", is(user.getEmail())))
                .andReturn();
    }

    @Test
    @WithMockCustomUser
    void getUserByExternalId_roleUser_succeed() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(UserController.ROOT_RESOURCE + "/" + user.getExternalId()))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.id", is(user.getId().intValue())))
                .andExpect(jsonPath("$.username", is(user.getUsername())))
                .andExpect(jsonPath("$.email", is(user.getEmail())))
                .andReturn();
    }

    @Test
    @WithMockCustomUser
    void getUserByExternalId_forbidden() throws Exception {
        // GIVEN
        User user = userRepository.save(UserFixture.buildUser());

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(UserController.ROOT_RESOURCE + "/" + UUID.randomUUID()))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createUser() throws Exception {
        // GIVEN
        UserDto userDto = UserFixture.buildUserDto();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(UserController.ROOT_RESOURCE)
                .content(mapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.username", is(userDto.getUsername())))
                .andExpect(jsonPath("$.email", is(userDto.getEmail())))
                .andExpect(jsonPath("$.role", is(ERole.ROLE_USER.name())))
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "USER")
    void createUser_forbidden() throws Exception {
        // GIVEN
        UserDto userDto = UserFixture.buildUserDto();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(UserController.ROOT_RESOURCE)
                .content(mapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createUser_usernameExists_badRequest() throws Exception {
        // GIVEN
        userRepository.save(UserFixture.buildUser());
        UserDto userDto = UserFixture.buildUserDto();
        userDto.setEmail("mailmail@mail.com");

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(UserController.ROOT_RESOURCE)
                .content(mapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(result -> Assertions.assertThat(result.getResponse().getErrorMessage()).isEqualTo("Le nom d'utilisateur " + userDto.getUsername() + " est déjà utilisé"))
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createUser_emailExists_badRequest() throws Exception {
        // GIVEN
        userRepository.save(UserFixture.buildUser());
        UserDto userDto = UserFixture.buildUserDto();
        userDto.setUsername("username458");

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(UserController.ROOT_RESOURCE)
                .content(mapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(result -> Assertions.assertThat(result.getResponse().getErrorMessage()).isEqualTo("L'adresse email " + userDto.getEmail() + " est déjà utilisée par un autre compte"))
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createUser_UsernameValidationException() throws Exception {
        // GIVEN
        UserDto user = UserDto.builder().email("eqfqde@email.com").password("password").build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(UserController.ROOT_RESOURCE)
                .content(mapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.fieldName", is("username")))
                .andExpect(jsonPath("$.message", is(ValidationErrorMessage.USERNAME_REQUIRED)))
                .andReturn();

        // GIVEN
        user.setUsername("demplokijuhygtfrdeszq");

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(UserController.ROOT_RESOURCE)
                .content(mapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.fieldName", is("username")))
                .andExpect(jsonPath("$.message", is(ValidationErrorMessage.USERNAME_SIZE)))
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createUser_EmailValidationException() throws Exception {
        // GIVEN
        UserDto user = UserDto.builder().username("username").password("password").build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(UserController.ROOT_RESOURCE)
                .content(mapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.fieldName", is("email")))
                .andExpect(jsonPath("$.message", is(ValidationErrorMessage.EMAIL_REQUIRED)))
                .andReturn();

        // GIVEN
        user.setEmail("demplokijuhygtfrdeszq");

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(UserController.ROOT_RESOURCE)
                .content(mapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.fieldName", is("email")))
                .andExpect(jsonPath("$.message", is(ValidationErrorMessage.EMAIL_PATTERN)))
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createUser_PasswordValidationException() throws Exception {
        // GIVEN
        UserDto user = UserDto.builder().username("username").email("email@email.com").build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(UserController.ROOT_RESOURCE)
                .content(mapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.fieldName", is("password")))
                .andExpect(jsonPath("$.message", is(ValidationErrorMessage.PASSWORD_REQUIRED)))
                .andReturn();

        // GIVEN
        user.setPassword("demplokijuhygtfrdeszq");

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(UserController.ROOT_RESOURCE)
                .content(mapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.fieldName", is("password")))
                .andExpect(jsonPath("$.message", is(ValidationErrorMessage.PASSWORD_SIZE)))
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateUser() throws Exception {
        // GIVEN
        User user = userRepository.save(UserFixture.buildUser());
        UserDto userDto = UserFixture.buildUserDto();
        userDto.setId(user.getId());
        userDto.setUsername("username6");
        userDto.setRole(ERole.ROLE_USER);

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.put(UserController.ROOT_RESOURCE + "/" + userDto.getId())
                .content(mapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.id", is(userDto.getId().intValue())))
                .andExpect(jsonPath("$.username", is(userDto.getUsername())))
                .andReturn();
    }

    @Test
    @WithMockCustomUser
    void updateUser_roleUser_succeed() throws Exception {
        // GIVEN
        UserDto userDto = UserDto.builder()
                .id(user.getId())
                .externalId(user.getExternalId())
                .username("username6")
                .password(user.getPassword())
                .role(user.getRole())
                .email(user.getEmail())
                .build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.put(UserController.ROOT_RESOURCE + "/" + userDto.getId())
                .content(mapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.id", is(userDto.getId().intValue())))
                .andExpect(jsonPath("$.username", is(userDto.getUsername())))
                .andReturn();
    }

    @Test
    @WithMockCustomUser
    void updateUser_forbidden() throws Exception {
        // GIVEN
        User user = userRepository.save(UserFixture.buildUser());
        UserDto userDto = UserFixture.buildUserDto();
        userDto.setId(user.getId());
        userDto.setUsername("username6");
        userDto.setRole(ERole.ROLE_USER);
        userDto.setExternalId(user.getExternalId());

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.put(UserController.ROOT_RESOURCE + "/" + user.getId())
                .content(mapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateUser_usernameExists_badRequest() throws Exception {
        // GIVEN
        List<User> users = userRepository.saveAll(UserFixture.buildUserList());
        UserDto userDto = UserFixture.buildUserDto();
        userDto.setId(users.get(0).getId());
        userDto.setUsername("username2");

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.put(UserController.ROOT_RESOURCE + "/" + userDto.getId())
                .content(mapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(result -> Assertions.assertThat(result.getResponse().getErrorMessage()).isEqualTo("Le nom d'utilisateur " + userDto.getUsername() + " est déjà utilisé"))
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateUser_emailExists_badRequest() throws Exception {
        // GIVEN
        List<User> users = userRepository.saveAll(UserFixture.buildUserList());
        UserDto userDto = UserFixture.buildUserDto();
        userDto.setId(users.get(0).getId());
        userDto.setEmail("email2@email.com");

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.put(UserController.ROOT_RESOURCE + "/" + userDto.getId())
                .content(mapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(result -> Assertions.assertThat(result.getResponse().getErrorMessage()).isEqualTo("L'adresse email " + userDto.getEmail() + " est déjà utilisée par un autre compte"))
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteUser() throws Exception {
        // GIVEN
        User user = userRepository.save(UserFixture.buildUser());

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.delete(UserController.ROOT_RESOURCE + "/" + user.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "VIEWER")
    void deleteUser_forbidden() throws Exception {
        // GIVEN
        User user = userRepository.save(UserFixture.buildUser());

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.delete(UserController.ROOT_RESOURCE + "/" + user.getId()))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }
}
