package com.openclassrooms.parentapp.gestioncompte.fixture;

import com.openclassrooms.parentapp.gestioncompte.entity.Child;
import com.openclassrooms.parentapp.gestioncompte.entity.User;

import java.time.LocalDate;
import java.util.List;

public class ChildFixture {

    public static List<Child> buildChildList(User user) {
        Child child1 = Child.builder()
                .name("child 1")
                .birthDate(LocalDate.now())
                .parent(user)
                .build();
        Child child2 = Child.builder()
                .name("child 2")
                .birthDate(LocalDate.now().plusDays(2))
                .parent(user)
                .build();

        return List.of(child1, child2);
    }

    public static Child buildChild(User user) {
        return Child.builder()
                .name("child 1")
                .birthDate(LocalDate.now())
                .parent(user)
                .build();
    }
}
