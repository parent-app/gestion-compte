package com.openclassrooms.parentapp.gestioncompte.fixture;

import com.openclassrooms.parentapp.gestioncompte.dto.UserDto;
import com.openclassrooms.parentapp.gestioncompte.dto.UserDtoWithoutPassword;
import com.openclassrooms.parentapp.gestioncompte.entity.ERole;
import com.openclassrooms.parentapp.gestioncompte.entity.User;

import java.util.List;
import java.util.UUID;

public class UserFixture {

    public static User buildUser() {
        return User.builder()
                .role(ERole.ROLE_USER)
                .email("email@email.com")
                .username("username")
                .password("password")
                .externalId(UUID.randomUUID())
                .build();
    }

    public static UserDto buildUserDto() {
        return UserDto.builder()
                .email("email@email.com")
                .username("username")
                .password("password")
                .build();
    }

    public static List<User> buildUserList() {
        User user1 = User.builder()
                .role(ERole.ROLE_USER)
                .email("email1@email.com")
                .username("username1")
                .password("password1")
                .build();
        User user2 = User.builder()
                .role(ERole.ROLE_USER)
                .email("email2@email.com")
                .username("username2")
                .password("password2")
                .build();
        User user3 = User.builder()
                .role(ERole.ROLE_USER)
                .email("email3@email.com")
                .username("username3")
                .password("password2")
                .build();
        return List.of(user1, user2, user3);
    }

    public static List<UserDtoWithoutPassword> buildUserDtoWithoutPasswordList() {
        UserDtoWithoutPassword user1 = UserDtoWithoutPassword.builder()
                .role(ERole.ROLE_USER)
                .email("email1@email.com")
                .username("username1")
                .build();
        UserDtoWithoutPassword user2 = UserDtoWithoutPassword.builder()
                .role(ERole.ROLE_USER)
                .email("email2@email.com")
                .username("username2")
                .build();
        UserDtoWithoutPassword user3 = UserDtoWithoutPassword.builder()
                .role(ERole.ROLE_USER)
                .email("email3@email.com")
                .username("username3")
                .build();
        return List.of(user1, user2, user3);
    }
}
